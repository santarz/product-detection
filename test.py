import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tf_explain.core.activations import ExtractActivations
from tensorflow.keras.applications.xception import decode_predictions
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.preprocessing import image
import os
import csv

# image folder
FOLDER_PATH = 'test'
# dimensions of images
IMG_SIZE = (299, 299)
BATCH_SIZE = 64

# load all images into a list
images = []
data = []
for img in os.listdir(FOLDER_PATH):
    data.append(img)
    img = os.path.join(FOLDER_PATH, img)
    img = image.load_img(img, target_size=IMG_SIZE, interpolation="nearest")
    img = image.img_to_array(img)
    img = np.expand_dims(img, axis=0)
    images.append(img)

# stack up images list to pass for prediction
images = np.vstack(images)

model = tf.keras.models.load_model("my_model")
model.compile(
    optimizer='adam',
    loss='sparse_categorical_crossentropy',
    metrics=['accuracy']
)

classes = model.predict_classes(images, batch_size=BATCH_SIZE)
padding = []
for c in classes:
    padding.append("{:02d}".format(c))

result = list(zip(data, padding))


# writing the data into the file
file = open('output.csv', 'w+', newline='')
with file:
    write = csv.writer(file)
    write.writerow(["filename", "category"])
    write.writerows(result)
