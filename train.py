import numpy as np
import tensorflow as tf
from tf_explain.core.activations import ExtractActivations
from tensorflow.keras.applications.xception import decode_predictions
from tensorflow.keras.layers import Dense, Flatten

IMG_SIZE = (299, 299)
BATCH_SIZE = 64
EPOCHS = 10

train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    'train',
    labels="inferred",
    label_mode="int",
    color_mode="rgb",
    image_size=IMG_SIZE,
    shuffle=True,
    batch_size=BATCH_SIZE,
    validation_split=0.2,
    subset="training",
    interpolation="nearest",
    follow_links=False,
    seed=27,
)

val_ds = tf.keras.preprocessing.image_dataset_from_directory(
    'train',
    labels="inferred",
    label_mode="int",
    color_mode="rgb",
    image_size=IMG_SIZE,
    shuffle=True,
    batch_size=BATCH_SIZE,
    validation_split=0.2,
    subset="validation",
    interpolation="nearest",
    follow_links=False,
    seed=27,
)

train_ds = train_ds.prefetch(buffer_size=BATCH_SIZE)
val_ds = val_ds.prefetch(buffer_size=BATCH_SIZE)

# load pre trained Xception model
pretrained_model = tf.keras.applications.xception.Xception(input_shape=[299, 299, 3],
                                                           weights='imagenet', include_top=False)
pretrained_model.trainable = False
pretrained_model.summary()

# define new model
model = tf.keras.Sequential([
    pretrained_model,
    Flatten(),
    Dense(1024, activation='relu'),
    Dense(42, activation='softmax')
])

model.summary()

model.compile(
    optimizer='adam',
    loss='sparse_categorical_crossentropy',
    metrics=['accuracy']
)
model.fit(
    train_ds, epochs=EPOCHS, batch_size=BATCH_SIZE, validation_data=val_ds
)

model.save("my_model")
